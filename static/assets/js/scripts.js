function navToggle(){
    $(document).on('click','.nav_toggle', function(){
        $('body').toggleClass("nav_opened");
    });
}
//function programaToggle(){
//    $(document).on('click','.show_programa', function(){
//        $('.content_programa').toggleClass("opened");
//        $(this).toggleClass("active");
//    });
//}
function faqToggle(){
    $(document).on('click','.faq .tab', function(){
        $('.faq').removeClass("opened");
        $(this).parent().addClass("opened");
    });
}
function odd_even(){
    var circles = $('.responses_container .circle');
    if(circles.length!==0){
        for(i=0;i<=circles.length;i++){
            if(i%2 !== 0){
                $(circles[i]).addClass('odd');
            }else{
                $(circles[i]).addClass('even');
            }
        }
    }
}

function send_message_callback(data){
    console.log(data);
    $('div.mesaj_eroare').remove();
    if(data.validation_errors){
        $.each(data.validation_errors, function(prop, value) {      
            $('#contact #'+prop).parent().append('<div class="mesaj_eroare">'+value+'</div>');   
        });
    }else if(data.response==='success'){
        $('.mesaj_succes').text('Mesajul a fost trimis!');
    }else if(data.response==='error'){ 
        $('.form').append('<div class="mesaj_eroare">A aparut o eroare in trimiterea datelor! Te rugam sa incerci din nou.</div>');
    }else if(data.response==='db_error'){ 
        $('.form').append('<div class="mesaj_eroare">A aparut o eroare in trimiterea datelor! Te rugam sa incerci din nou.</div>');
    }else if(data.response==='error_form'){ 
        $('.form').append('<div class="mesaj_eroare">A aparut o eroare in preluarea datelor! Te rugam sa incerci din nou.</div>');
    }
}

function send_registration_callback(data){
//    console.log(data);
    $('div.mesaj_eroare').remove();
    var course_date = $('#register_course select[name=course_date]').find(":selected").val();
//    var date = new Date(course_date.substring(0, 10)); console.log(date);
//    date.setDate(date.getDate(course_date.substring(0, 10)) - 5);
//    var expirationDateString = date.toISOString().split('T')[0];
    var twoDigitDate = course_date.substring(0, 2); if(twoDigitDate.length===1)	twoDigitDate="0" +twoDigitDate;
    
    if(data.validation_errors){
        $.each(data.validation_errors, function(prop, value) { 
            $('#register_course #'+prop).parent().append('<div class="mesaj_eroare">'+value+'</div>');
        });
    }else if(data.response==='success'){
        $('.form').css('display', 'none');
        $('.curs_modal_box').css('display', 'none');
        var success_msg = '<div class="mesaj_succes">Mesajul a fost trimis!</div>'+
                          '<div class="success_msg">'+
                                '<p>V-ați rezervat un loc la cursul din data de '+ course_date.substring(0, 10) +', ora '+ course_date.substring(11) +'.</p>'+
                                '<p>Pentru a finaliza înscrierea, plătiți în contul IBAN RO72 BTRL RONC RT03 0212 6201, deschis la Banca Transilvania, al societății Wise Projects Development SRL, CUI34633627 și J40/7037/2015, suma de 500 lei reprezentând contravaloarea cursului pentru <i>CURS HTML/CSS</i>. </p>'+
                                '<p>În cazul neachitării până la data de '+ (twoDigitDate-5)+ course_date.substring(2, 10) +', rezervarea se consideră nulă.</p>'+
                            '<br>'+
                            '<br>'+
                            'Echipa Invată Programare'+
                          '</div>';
        $('.modal-content').append(success_msg);
    }else if(data.response==='error'){ 
        $('.form').append('<div class="mesaj_eroare">A aparut o eroare in trimiterea datelor! Te rugam sa incerci din nou.</div>');
    }else if(data.response==='db_error'){ 
        $('.form').append('<div class="mesaj_eroare">A aparut o eroare in trimiterea datelor! Te rugam sa incerci din nou.</div>');
    }else if(data.response==='error_form'){ 
        $('.form').append('<div class="mesaj_eroare">A aparut o eroare in preluarea datelor! Te rugam sa incerci din nou.</div>');
    }
}

    //api call function
    function request(url, type, data, callback) {
        data = typeof data !== 'undefined' ? data : {};
        type = typeof type !== 'undefined' ? type : "GET";
        callback = typeof callback !== 'undefined' ? callback : false;
        var jqxhr = $.ajax({
                url: url,
                type: type,
                data: data,
                dataType: "json",
                beforeSend: function (xhr) {
                    console.log('submitting');
                }
            })
            .done(function (result) {
                if (callback !== false && typeof callback === "function") {
                    callback(result, data);
                }else{
                    return result;
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log('error' + textStatus + ' ' + errorThrown);
                $('div.mesaj_eroare').remove();
                $('.form').append('<div class="mesaj_eroare">A aparut o eroare in trimiterea datelor! Te rugam sa incerci din nou, mai tarziu.</div>');
            })
            .always(function () {
                console.log('complete');
            });
        return jqxhr.responseText;
    }

jQuery(document).ready( function($) {
    navToggle();
    //programaToggle();
    faqToggle();
    odd_even();
});

$('body').on("click", "#send-message", function(e){
    e.preventDefault();
    var formData = {
            'message'       : $('#contact textarea[name=message]').val(),
            'first_name'    : $('#contact input[name=first_name]').val(),
            'last_name'     : $('#contact input[name=last_name]').val(),
            'email'         : $('#contact input[name=email]').val(),
            'phone'         : $('#contact input[name=phone]').val()
        };
    request(SITE_URL + 'main/contact_process', 'POST', formData, send_message_callback);
});

$('body').on("click", "#send-registration", function(e){
    e.preventDefault();
    var formData = {
            'first_name'  : $('#register_course input[name=first_name]').val(),
            'last_name'   : $('#register_course input[name=last_name]').val(),
            'day'         : $('#register_course select[name=day]').find(":selected").val(),
            'month'       : $('#register_course select[name=month]').find(":selected").val(),
            'year'        : $('#register_course select[name=year]').find(":selected").val(),
            'sex'         : $('#register_course input[name=sex]:checked').val(),
            'email'       : $('#register_course input[name=email]').val(),
            'phone'       : $('#register_course input[name=phone]').val(),
            'course_date' : $('#register_course select[name=course_date]').find(":selected").val()
        };
    request(SITE_URL + 'main/register_process', 'POST', formData, send_registration_callback);
});

$('#modalContact, #modalInscriere').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input:not(:checkbox):not(:radio),textarea")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end()
    .find("div.mesaj_eroare").empty()
        .end()
    .find("div.mesaj_succes").empty()
        .end()
    .find('#course_date').val('').trigger('change');
});

$('#modalContact, #modalInscriere').on('show.bs.modal', function (e) {
   var time =  $(e.relatedTarget).data('time');
   if(time !== undefined){
        $('#course_date').val(time).trigger('change');
   }
});

//For checkbox focus in and out 
$(document).on('focus', '.label_checkbox', function(e) {
  $(this).addClass('checkbox_focus');
});
$(document).on('focusout', '.label_checkbox', function(e) {
  $(this).removeClass('checkbox_focus');
});
