<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Partystarter model
 * @author Vlad
 */
class Model_invata_programare extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    /**
     * Insert contact form message to DB
     */
    public function add_contact_msg($last_name, $first_name, $phone, $email, $message){
        
        $this->db->insert('contact_form', array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'phone' => $phone, 'message' => $message, 'registration_time' => date("Y-m-d H:i:s")));
        $result = $this->db->insert_id();
        
        return $result;
    }
    
    /**
     * Insert course registration form message to DB
     */
    public function add_course_registration($last_name, $first_name, $birth_date, $phone, $email, $sex, $course_date){
        
        $this->db->insert('register_form', array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'phone' => $phone, 'birth_date' => $birth_date, 'sex' => $sex, 'course_date' => $course_date, 'registration_time' => date("Y-m-d H:i:s")));
        $result = $this->db->insert_id();
        
        return $result;
    }
    
}
