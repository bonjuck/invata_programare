<?php

$lang['form_validation_required']			= 'Campul <i>%s</i> este obligatoriu.';
$lang['form_validation_isset']				= 'Campul <i>%s</i> este obligatoriu.';
$lang['form_validation_valid_email']		= 'Campul <i>%s</i> trebuie completat cu o adresa valida de email.';
$lang['form_validation_valid_emails']		= 'Campul <i>%s</i> trebuie completat cu adrese valide de email.';
$lang['form_validation_valid_url']			= 'Campul <i>%s</i> field must contain a valid URL.';
$lang['form_validation_valid_ip']			= 'Campul <i>%s</i> field must contain a valid IP.';
$lang['form_validation_min_length']			= 'Campul <i>%s</i> trebuie sa contina minim <i>%s</i> caractere.';
$lang['form_validation_max_length']			= 'Campul <i>%s</i> nu poate depași <i>%s</i> caractere.';
$lang['form_validation_exact_length']		= 'Campul <i>%s</i> trebuie sa contina <i>%s</i> caractere.';
$lang['form_validation_alpha']				= 'Campul <i>%s</i> poate sa contina doar litere.';
$lang['form_validation_alpha_numeric']		= 'Campul "<i>%s</i>" poate sa contina doar litere si cifre.';
$lang['form_validation_alpha_dash']			= 'Campul <i>%s</i> poate sa contina doar litere, cifre, _ si -.';
$lang['form_validation_numeric']			= 'Campul <i>%s</i> poate sa contina doar numere.';
$lang['form_validation_is_numeric']			= 'Campul <i>%s</i> poate sa contina doar cifre.';
$lang['form_validation_integer']			= 'Campul <i>%s</i> poate sa contina doar un numar întreg.';
$lang['form_validation_regex_match']		= 'Campul <i>%s</i> nu este in formatul corespunzator.';
$lang['form_validation_matches']			= 'Campul <i>%s</i> trebuie sa fie identic cu textul din Campul <i>%s</i>.';
$lang['form_validation_is_unique'] 			= 'Campul <i>%s</i> trebuie sa contina o valoare unică.';
$lang['form_validation_is_natural']			= 'Campul <i>%s</i> poate sa contina doar un numar natural.';
$lang['form_validation_is_natural_no_zero']	=  'Campul <i>%s</i> trebuie sa contina un numar natural mai mare ca zero.';
$lang['form_validation_decimal']			= 'Campul <i>%s</i> trebuie sa contina un numar decimal.';
$lang['form_validation_less_than']			= 'Campul <i>%s</i> trebuie sa contina un numar mai mic ca <i>%s</i>.';
$lang['form_validation_greater_than']		= 'Campul <i>%s</i> trebuie sa contina un numar mai mare ca <i>%s</i>.';
$lang['form_validation_alpha_dash_space']		= 'Campul <i>%s</i> poate sa contina doar litere si spatii.';
$lang['form_validation_differs']		= 'Nu poti juca singur. Invita un prieten!';

//ș ț ă î â
/* End of file form_validation_lang.php */
/* Location: ./app/language/ro/form_validation_lang.php */
