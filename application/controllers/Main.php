<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Index(Home) Page controller.
     */
    public function index(){
        $this->content_view = 'home';
        $this->load->view('templates/default'); 
    }
    
    /**
     * Why page.
     */
    public function why(){
        $this->content_view = 'why';
        $this->load->view('templates/default');
    }
    
    /**
     * Learning schedule page.
     */
    public function learning_schedule(){
        $this->content_view = 'learning_schedule';
        $this->load->view('templates/default');
    }
    
    /**
     * Trainers Page.
     */
    public function trainers(){
        $this->content_view = 'trainers';
        $this->load->view('templates/default');
    }
    
    /**
     * Experts Page.
     */
    public function experts(){
        $this->content_view = 'experts';
        $this->load->view('templates/default');
    }
    
    /**
     * Partners Page.
     */
    public function partners(){
        $this->content_view = 'partners';
        $this->load->view('templates/default');
    }
    
    /**
     * Faq Page.
     */
    public function faq(){
        $this->content_view = 'faq';
        $this->load->view('templates/default');
    }
    
    /**
     * Terms Page.
     */
    public function terms(){
        $this->content_view = 'terms';
        $this->load->view('templates/default');
    }
    
    /**
     * Privacy Page.
     */
    public function privacy(){
        $this->content_view = 'privacy';
        $this->load->view('templates/default');
    }
    
    /**
     * Trainers Page.
     */
    public function calendar(){
        $this->content_view = 'calendar';
        $this->load->view('templates/default');
    }
    
    
    /**
     * Proces contact form.
     */
    public function contact_process(){
        
        $data = array();
        
        $this->load->library('form_validation');

        if($this->form_validation->run() === FALSE){

            $errors['validation_errors'] =  $this->form_validation->error_array();
            //retun data to ajax call
            print_r(json_encode($errors));
        } else {
            //get and sort inputs acording to form type
            $last_name = $this->input->post('last_name');
            $first_name = $this->input->post('first_name');
            $email  = $this->input->post('email'); 
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');

            //log the form data to DB
            $sender = $this->Model_invata_programare->add_contact_msg($last_name, $first_name, $phone, $email, $message);
                //if logging to DB ok send email
                if(is_numeric($sender)){
                    $this->form_validation->reset_validation();
                    $sent = $this->send_contact_email($last_name, $first_name, $phone, $email, $message);
                    if($sent == 'sent'){
                        $data['response'] = 'success'; //the emails are sent
                    }else{
                        $data['response'] = 'error'; //error on sending emails
                    }      
                }else{ 
                    $data['response'] = 'db_error'; //data enterred is incorect
                }
            //retun data to ajax call    
            print_r(json_encode($data));
        }  
    }
    
    /**
     * Process register form.
     */
    public function register_process(){
        
        $data = array();
        
        $this->load->library('form_validation');

        if($this->form_validation->run() === FALSE){

            $errors['validation_errors'] =  $this->form_validation->error_array();
            //retun data to ajax call
            print_r(json_encode($errors));
        } else {
            //get and sort inputs acording to form type
            $last_name = $this->input->post('last_name');
            $first_name = $this->input->post('first_name');
            $email  = $this->input->post('email'); 
            $phone = $this->input->post('phone');
            $birth_date = $this->input->post('day').'-'
                          .$this->input->post('month').'-'
                          .$this->input->post('year');
            $sex = $this->input->post('sex');
            $course_date = $this->input->post('course_date');
            
            //log the form data to DB
            $sender = $this->Model_invata_programare->add_course_registration($last_name, $first_name, $birth_date, $phone, $email, $sex, $course_date);
                //if logging to DB ok send email
                if(is_numeric($sender)){
                    $this->form_validation->reset_validation();
                    $sent = $this->send_registration_email($last_name, $first_name, $phone, $email, $course_date);
                    if($sent == 'sent'){
                        $data['response'] = 'success'; //the emails are sent
                    }else{
                        $data['response'] = 'error'; //error on sending emails
                    }      
                }else{ 
                    $data['response'] = 'db_error'; //data enterred is incorect
                }
            //retun data to ajax call    
            print_r(json_encode($data));
        }  
    }
    
    /**
     * Send contact email
     */
    private function send_contact_email($last_name, $first_name, $phone, $email, $message){
        $sent = NULL;
        $this->load->library('email');
        $from_email = 'noreply@invataprogramare.ro'; //$this->email->smtp_user
        
        $data['last_name']       = $last_name;
        $data['first_name']      = $first_name;
        $data['phone']           = $phone;
        $data['email']           = $email;
        $data['message']         = $message;  
        
        //Send email to the customer
        $email_msg_cust = $this->load->view('templates/email_tpl_contact_cust', $data, TRUE);
        $this->email->from($from_email, 'Invata Programare');
        $this->email->to($email); //', office@ascendis.ro'
        $this->email->subject('Contact Invata Programare');
        $this->email->message($email_msg_cust);
        if ($this->email->send()){   
            $sent_cust = 1;
        } else {
            $sent_cust = 0;
        }
        
        //Send email to us
        $email_msg_skill = $this->load->view('templates/email_tpl_contact_us', $data, TRUE);
        $this->email->clear();
        $this->email->from($from_email, 'Invata Programare');
        $this->email->to('aurpur@gmail.com, vladimir.cioaca@advancedideas.ro, adrian.apostol@advancedideas.ro, adrian.apostol@ideologiq.com, cornel@invataprogramare.ro, adrian@invataprogramare.ro, vlad@invataprogramare.ro, contact@invataprogramare.ro'); //email invata programamre
        $this->email->subject('Contact Invata Programare');
        $this->email->message($email_msg_skill); 
        if ($this->email->send()){   
            $sent_us = 1;
        } else {
            $sent_us = 0;
        }
        //verify if both emails have been sent
        if ($sent_cust == 1 && $sent_us == 1){   
            // mail sent
            $sent = 'sent';
        } else {
            //error sending mail
            $sent = 'error';
        }
        
        return $sent;
    }
    /**
     * Send registration email
     */
    private function send_registration_email($last_name, $first_name, $phone, $email, $course_date){
        $sent = NULL;
        $this->load->library('email');
        $from_email = 'noreply@invataprogramare.ro'; //$this->email->smtp_user
        
        $data['last_name']       = $last_name;
        $data['first_name']      = $first_name;
        $data['phone']           = $phone;
        $data['email']           = $email;
        $data['course_date']     = $course_date;   
        
        //Send email to the customer
        $email_msg_cust = $this->load->view('templates/email_tpl_register_cust', $data, TRUE);
        $this->email->from($from_email, 'Invata Programare');
        $this->email->to($email); //', office@ascendis.ro'
        $this->email->subject('Contact Invata Programare');
        $this->email->message($email_msg_cust);
        if ($this->email->send()){   
            $sent_cust = 1;
        } else {
            $sent_cust = 0;
        }
        
        //Send email to us
        $email_msg_skill = $this->load->view('templates/email_tpl_register_us', $data, TRUE);
        $this->email->clear();
        $this->email->from($from_email, 'Invata Programare');
        $this->email->to('aurpur@gmail.com, vladimir.cioaca@advancedideas.ro, adrian.apostol@advancedideas.ro, adrian.apostol@ideologiq.com, cornel@invataprogramare.ro, adrian@invataprogramare.ro, vlad@invataprogramare.ro, contact@invataprogramare.ro'); //email invata programamre
        $this->email->subject('Contact Invata Programare');
        $this->email->message($email_msg_skill); 
        if ($this->email->send()){   
            $sent_us = 1;
        } else {
            $sent_us = 0;
        }
        //verify if both emails have been sent
        if ($sent_cust == 1 && $sent_us == 1){   
            // mail sent
            $sent = 'sent';
        } else {
            //error sending mail
            $sent = 'error';
        }
        
        return $sent;
    }
}
