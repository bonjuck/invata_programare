<?php

/* 
 * Form validation rules set
 */
$config = array(
        'main/contact_process' => array(
                array(
                        'field' => 'last_name',
                        'label' => 'Nume',
                        'rules' => 'trim|required|regex_match[/^[a-zA-Z -]+$/]'
                ),
                array(
                        'field' => 'first_name',
                        'label' => 'Prenume',
                        'rules' => 'trim|required|regex_match[/^[a-zA-Z -]+$/]'
                ),
                array(
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'trim|valid_email|required'
                ),
                array(
                        'field' => 'phone',
                        'label' => 'Telefon',
                        'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|regex_match[/^(?:0?)([7]\d{1})(\d{7})$/]'
                ),
                array(
                        'field' => 'message',
                        'label' => 'Mesaj',
                        'rules' => 'trim|required|alpha_numeric_spaces'
                )
        ),
        'main/register_process' => array(
                array(
                        'field' => 'last_name',
                        'label' => 'Nume',
                        'rules' => 'trim|required|regex_match[/^[a-zA-Z -]+$/]'
                ),
                array(
                        'field' => 'first_name',
                        'label' => 'Prenume',
                        'rules' => 'trim|required|regex_match[/^[a-zA-Z -]+$/]'
                ),
                array(
                        'field' => 'day',
                        'label' => 'Zi',
                        'rules' => 'trim|required|numeric'
                ),
                array(
                        'field' => 'month',
                        'label' => 'Luna',
                        'rules' => 'trim|required|numeric'
                ),
                array(
                        'field' => 'year',
                        'label' => 'An',
                        'rules' => 'trim|required|numeric'
                ),
                array(
                        'field' => 'sex',
                        'label' => 'Sex',
                        'rules' => 'required|max_length[1]'
                ),
                array(
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'trim|valid_email|required'
                ),
                array(
                        'field' => 'phone',
                        'label' => 'Telefon',
                        'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|max_length[10]|regex_match[/^(?:0?)([7]\d{1})(\d{7})$/]'
                ),
                array(
                        'field' => 'course_date',
                        'label' => 'Perioada cursului',
                        'rules' => 'trim|required|regex_match[/^[a-zA-Z0-9 -:.]+$/]'
                )
        ),
);

