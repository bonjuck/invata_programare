<body class="<?php if($this->content_view == 'home') { echo 'is_home'; } ?>">
<div class="main_wrapper">
    <header>
        <div class="main_header">
            <div class="container clearfix">
                <div class="logo"><a href="/"><img src="<?php echo site_url('static/assets/images/logo-invata-programare.svg') ?>"></a></div>
                <div class="nav_button nav_toggle"><span></span></div>
                <div class="main_menu">
                    <div class="align_left">
                        <a href="<?php echo site_url('de-ce') ?>" class="<?php echo $this->content_view == 'why' ? 'active' : '' ?>">De ce?</a>
                        <a href="<?php echo site_url('programa') ?>" class="<?php echo $this->content_view == 'learning_schedule' ? 'active' : '' ?>">Programă curs</a>
                        <a href="<?php echo site_url('traineri') ?>" class="<?php echo $this->content_view == 'trainers' ? 'active' : '' ?>">Traineri</a>
                        <a href="<?php echo site_url('experti') ?>" class="<?php echo $this->content_view == 'experts' ? 'active' : '' ?>">Experți</a>
                        <a href="<?php echo site_url('parteneri') ?>" class="<?php echo $this->content_view == 'partners' ? 'active' : '' ?>">Parteneri</a>
                        <!--<a href="<?php // echo site_url('faq') ?>" class="<?php // echo $this->content_view == 'faq' ? 'active' : '' ?>">FAQ </a>-->
    <!--                    <a href="#"><i class="icon-facebook"></i></a>-->
                    </div>
                    <div class="align_right">
                        <a href="#" data-toggle="modal" data-target="#modalInscriere" class="bt_pink">Vreau să mă înscriu <i class="icon-right"></i></a>
                    </div>
                </div>  
            </div>
        </div>
    </header>
    <!--end header-->