</div>
<!--footer-->
<footer class="footer">
    <div class="modal fade" id="modalInscriere" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="bt_close" data-dismiss="modal"><i class="icon-up-open"></i></div>
                <div class="modal_title">Formular de înscriere</div>
                <div class="curs_modal_box">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="curs_name">CURS HTML/CSS</div>
                            <div class="curs_time">Durata: <strong>10 zile</strong></div>
                        </div>
                        <div class="col-sm-5">
                            <div class="curs_price">
                                <div class="curs_price_label">PREȚ PROMO</div>
                                500<span>RON</span>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="form" name="register_course" id="register_course" method="post" action="<?php echo site_url('main/register_process'); ?>">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input_wrapper">
                                <!--<div class="mesaj_eroare">Campul este obligatoriu</div>-->
                                <input type="text" id="last_name" name="last_name" placeholder="Nume" value="<?php echo set_value('last_name'); ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_wrapper">
                                <input type="text" id="first_name" name="first_name" placeholder="Prenume" value="<?php echo set_value('first_name'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="clearfix selector_wrapper">
                                <div class="label_data">Data nașterii</div>
                                <div class="selectors">
                                    <select name="day" id="day" class="date_select" data-placeholder="zi" >
                                        <option></option>
                                        <?php for($i=1;$i<=31;$i++): ?>
                                        <option value="<?php echo $i ?>"><?php echo $i<10 ? '0'.$i : $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <select name="month" id="month" data-placeholder="luna" >
                                        <option></option>
                                        <?php for($i=1;$i<=12;$i++): ?>
                                        <option value="<?php echo $i ?>"><?php echo $i<10 ? '0'.$i : $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <select name="year" id="year" data-placeholder="an" >
                                        <option></option>
                                        <?php for($i=2002;$i>=1902;$i--): ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row checkboxes_wrapper">
                                <div class="col-xs-2">Sex</div>
                                <div class="col-xs-10" id="sex">
                                    <label class="label_checkbox"><input type="radio" name="sex" value="F">feminin<span></span></label>
                                    <label class="label_checkbox"><input type="radio" name="sex" value="M">masculin<span></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input_wrapper">
                                <input type="email" name="email" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_wrapper">
                                <input type="text" name="phone" id="phone" placeholder="Telefon (Format: 0798765432)" value="<?php echo set_value('phone'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="clearfix selector_wrapper">
                                <div class="select_perioada">
                                    <select name="course_date" id="course_date" data-placeholder="Alege perioada" >
                                        <option></option>
                                        <option value="30-10-2017 09:00">30.10 - 10.11 ora 09:00–13:00</option>
                                        <option value="30-10-2017 14:00">30.10 - 10.11 ora 14:00–18:00</option>
                                        <option value="13-11-2017 09:00">13.11 – 24.11 ora 09:00–13:00</option>
                                        <option value="13-11-2017 14:00">13.11 – 24.11 ora 14:00–18:00</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row top40">
                        <div class="col-sm-6">
                            <div class="text_appendix">* Un cursant se considera inscris in momentul incasarii platii cursului.</div>
<!--                            <div class="mesaj_succes"></div>-->
                        </div>
                        <div class="col-sm-6">
                            <a type="submit" id="send-registration" class="bt_pink pull-right">Mă înscriu <i class="icon-right"></i></a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
  <!--Contact form-->  
    <div class="modal fade" id="modalContact" >
        <div class="modal-dialog">
            <div class="modal-content contact_modal_content">
                <div class="bt_close" data-dismiss="modal"><i class="icon-up-open"></i></div>
                <div class="clearfix row-eq-height">
                    <div class="col-sm-6 form_side">
                        <div class="modal_title">Contact</div>
                        <form class="form" name="contact" id="contact" method="post" action="<?php echo site_url('main/contact_process'); ?>">
                            <div class="input_wrapper">    
                                <input type="text" name="last_name" id="last_name" placeholder="Nume" value="<?php echo set_value('last_name'); ?>">
                            </div>
                            <div class="input_wrapper">
                                <input type="text" name="first_name" id="first_name" placeholder="Prenume" value="<?php echo set_value('first_name'); ?>">
                            </div>
                            <div class="input_wrapper">
                                <input type="email" name="email" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
                            </div>
                            <div class="input_wrapper">
                                <input type="text" name="phone" id="phone" placeholder="Telefon" value="<?php echo set_value('phone'); ?>">
                            </div>
                            <div class="input_wrapper">
                                <textarea name="message" id="message" cols="30" rows="10" placeholder="Mesaj" value="<?php echo set_value('message'); ?>" ></textarea>
                            </div>
                            <div class="mesaj_succes"></div>
                            <a type="submit" id="send-message" class="bt_pink">Trimite <i class="icon-right"></i></a>
                        </form>
                    </div>
                    <div class="col-sm-6 address_side">
                        Relații si informații <br>
                        <strong>Corneliu Ionescu</strong> <br>
                        <strong>Tel: 0723.681.502</strong>
                        <div class="line"></div>
                        <strong>Wise Projects Development S.R.L.</strong> <br>
                        Str. Agatha Bârsescu nr. 27, Bl. V27A, Ap. 24<br>
                        Tel: 0723.681.502 <br>
                        <div class="line"></div>
                        <a href="https://goo.gl/maps/Ls1USBkeX5T2" class="bt_black" target="_blank">Vezi google map <i class="icon-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer_wrapper">
        <div class="row">
            <div class="col-md-7">
                <div class="linkuri">
                    <a href="<?php echo site_url('calendar') ?>" class="<?php echo $this->content_view == 'calendar' ? 'active' : '' ?>">Calendar</a>
                    <a href="http://anpc.gov.ro" target="_blank">ANPC</a>
    <!--                <a href="<?php // echo site_url('confidentialitate') ?>">Confidențialitate</a>-->
                    <a href="<?php echo site_url('termeni') ?>"  class="<?php echo $this->content_view == 'terms' ? 'active' : '' ?>">Temeni și condiții</a>
                    <a href="<?php echo site_url('faq') ?>" class="<?php echo $this->content_view == 'faq' ? 'active' : '' ?>">FAQ </a>
                    <a href="#" data-toggle="modal" data-target="#modalContact">Contact</a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="cu_sprijinul">
                    <span>Cu sprijinul:</span>
                    <a href="http://www.wisestartup.ro/" target="_blank" title="Wise Projects"><img src="<?php echo site_url('static/assets/images/logo_wise.png') ?>" alt="Wise Projects" target="_blank"></a>
                    <a href="https://www.ideologiq.com/" target="_blank" title="IdeologIQ"><img src="<?php echo site_url('static/assets/images/logo-ideologiq.png') ?>" alt="IdeologIQ" target="_blank"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="copy">Copyright © 2017 Invataprogramare All rights reserved</span>
            </div>
        </div>
    </div>
</footer>