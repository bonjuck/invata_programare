<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Invață Programare</title>
    
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url('/static/assets/images/apple-touch-icon.png');?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url('/static/assets/images/favicon-32x32.png');?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url('/static/assets/images/favicon-16x16.png');?>">
    <link rel="mask-icon" href="<?php echo site_url('/static/assets/images/safari-pinned-tab.svg');?>" color="#5bbad5">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=latin-ext" rel="stylesheet">
    <link href="<?php echo site_url('/static/assets/css/normalize.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url('/static/assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url('/static/assets/css/select2.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url('/static/assets/css/styles.css');?>" rel="stylesheet" type="text/css">
    <script> var SITE_URL = '<?php echo site_url() ?>'; </script>
</head>