<!--scripts-->
<script type="text/javascript" src="<?php echo site_url('static/assets/js/jquery-3.2.1.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('static/assets/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('static/assets/js/select2.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('static/assets/js/scripts.js') ?>"></script>
<script type="text/javascript">
    jQuery(document).ready( function($) {
        $("select").select2({
            minimumResultsForSearch: Infinity
        });
    });
</script>
<!--scripts-->
</body>
</html>