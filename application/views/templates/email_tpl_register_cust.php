Bună <?php echo $first_name.' '.$last_name; ?>! 
<br>
<br>
<p>V-ați rezervat un loc la cursul din data de <?php echo substr($course_date, 0, 10); ?>, ora <?php echo substr($course_date, 11); ?>.</p>
<p>Pentru a finaliza înscrierea, plătiți în contul IBAN RO72 BTRL RONC RT03 0212 6201, deschis la Banca Transilvania, al societății Wise Projects Development SRL, CUI34633627 și J40/7037/2015, suma de 500 lei reprezentând contravaloarea cursului pentru <i>CURS HTML/CSS</i>.</p> 
<p>În cazul neachitării până la data de <?php echo $date_string = date('d-m-Y',(strtotime ( '-5 days' , strtotime ( $course_date) ) )); ?>, rezervarea se consideră nulă.</p>
<br>
<br>
Echipa Invată Programare
<br>
<a href="https://www.invataprogramare.ro" title="Invata Programare">www.invataprogramare.ro</a> 