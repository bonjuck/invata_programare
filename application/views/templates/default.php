<?php
$this->load->view('partials/_head');

$this->load->view('partials/_header');

if (!empty($this->content_view)) {
    $this->load->view('content/' . $this->content_view);
}

$this->load->view('partials/_footer');

$this->load->view('partials/_footer_scripts');