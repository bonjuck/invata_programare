
<section class="section_content">
    <div class="container">
        <div class="row partner">
            <a href="http://www.wisestartup.ro/" target="_blank" title="Wise Projects">
                <div class="col-md-4 image">
                    <img src="<?php echo site_url('static/assets/images/logo_wise.png') ?>" alt="Wise Projects">
                </div>
            </a>
            <div class="col-md-8 description">
                <div class="name">WISE PROJECTS</div>
                <p>Wise Projects Development SRL creează și livrează traininguri și instrumente de training și consultanță pentru afaceri și antreprenori. </p>
                <p>A dezvoltat platforma de scriere colaborativă de planuri de afacere online wisestartup.ro, pe care o folosește în beneficiul clienților săi. </p>
                <p>Viziunea noastră în dezvoltarea programelor de formare este să livrăm experiențe cu aplicabilitate imediată în viața cursanților noștri, cu rezultate măsurabile și cu impact major în atingerea obiectivelor pe care și le-au propus.</p>
            </div>
        </div>
        <div class="row partner">
            <a href="https://www.ideologiq.com/" target="_blank" title="IdeologIQ">
                <div class="col-md-4 image">
                    <img src="<?php echo site_url('static/assets/images/logo-white_ideologiq.svg') ?>" alt="IdeologIQ">
                </div>
            </a>
            <div class="col-md-8 description">
                <div class="name">IDEOLOGIQ</div>
                <p>Ideologiq proiectează și furnizează soluții de comunicare și marketing care acoperă o întreaga gamă a mixului media disponibil: de la campanii online la cele offline,
                    de la activări BTL până la comunicare ATL, de la generarea identității până la producția media AV, de la design UI / UX la dezvoltarea software-ului complet,
                    de la clase și cursuri despre marcomms până la proiecte de digitizare majore livrate la cheie - 
                    întregul pachet! 
                </p>
                <p>Suntem mândri de propriile noastre procese interne care sunt ajutate de soluții software personalizate pentru a furniza rezultate măsurabile,
                    precum și de echipa noastră de specialiști de top în domeniile lor de expertiză.
                </p>
            </div>
        </div>

    </div>
</section>