
<section class="section_content">
    <div class="container">
        <div class="row trainer">
            <!--<div class="col-md-6 col-sm-12 trainer">--> 
                <!--<div class="row">-->
                    <div class="col-md-3 col-sm-4 col-lg-4 image">
                        <img src="<?php echo site_url('static/assets/images/Cornel_photo.jpg') ?>" alt="">
                    </div>
                    <div class="col-md-9 col-sm-8 col-lg-8 description">
                        <div class="name">Corneliu Ionescu</div>
        <!--                <div class="expertiza">TRAINER HTML</div>-->
                        <p>Cornel are o experienta de 10 ani in formare, este pasionat de maximizarea potentialului din 
                            oamenii pe care ii intalneste, ii place sa creeze lucruri.</p>
                        <p>Este fondator al scolii invataprogramare.ro, al wisestartup.ro, o platforma ce isi ajuta utilizatorii in scrierea planurilor de afaceri, 
                            precum si fondator al Asociatiei Educatie pentru Viata Reala, axata pe educatie financiara.  
                           Crede ca a programa e una din caile actuale de a crea si de a deveni independent financiar.</p>
                    </div>
                <!--</div>-->
            <!--</div>-->
        </div>    
        <div class="row trainer">
            <!--<div class="col-md-6 col-sm-12 trainer">-->
                <!--<div class="row">-->
                    <div class="col-md-3 col-sm-4 col-lg-4 image">
                        <img src="<?php echo site_url('static/assets/images/CVI_Lin.jpg') ?>" alt="">
                    </div>
                    <div class="col-md-9 col-sm-8 col-lg-8 description">
                        <div class="name">Cioaca Vladimir Ionut</div>
        <!--                <div class="expertiza">TRAINER</div>-->
                        <p> Are aproximativ 2 ani de zile de cand a intrat in Ideologiq Group (una dintre cele mai mari companii de digital independente din Romania).
                            A dezvoltat peste 40 de website-uri pentru clientii companiei, folosind tehologii precum HTML, Javascript si PHP.</p>
                        <p> Vladimir doreste sa ofere si altor persoane cunostintele acumulate in ultimii ani si este sigur ca in urma acestor training-uri multi cursanti 
                            vor fi pregatiti sa se angajeze in companii mari din Romania si din strainatate.
                        </p>
                    </div>
            <!--</div>-->
        </div>    
    </div>
</section>