
<section class="section_content">
    <div class="container">
        <div class="faq opened">
            <div class="tab">
                Cum a fost creată Școala de IT?
            </div>
            <div class="text">
                <p>Școala de IT este un proiect gândit și realizat de doi parteneri diferiți, dar complementari: agenția Ideologiq Group și Asociația Educație pentru Viată Reală. Aceștia vin cu know-how ce formează o bază solidă pentru formarea tinerilor în domeniul IT.</p>
                <p>Agenția Ideologiq Group este una dintre primele agenții independente de digital marketing din România, cu o experiență de peste 10 ani în crearea de website-uri, aplicații mobile și software personalizat pentru companii de top.</p>
                <p>Asociația Educație pentru Viată Reală este un ONG fondat din 2013 ce are ca scop principal formarea și educarea tinerilor.</p>
            </div>
        </div>
        <div class="faq">
            <div class="tab">
                Cum pot aplica?
            </div>
            <div class="text">Înscrierile la cursurile noastre se fac direct de pe site, prin intermediul formularului destinat fiecărui curs.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Cine va susține cursurile?
            </div>
            <div class="text">Cursurile vor fi susținute de Corneliu Ionescu și Vladimir Cioaca. Mai multe detalii despre ei poți găsi <a href="<?php echo site_url('traineri') ?>">aici</a>.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Sunteți acreditați pentru a ține cursuri?
            </div>
            <div class="text">Da, suntem prin unul din partenerii fondatori ai Școlii, Asociația Educație pentru Viată Reală.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Până când mă pot înscrie?
            </div>
            <div class="text">Datele limită de înscriere pentru fiecare curs sunt detaliate în pagina de descriere a acestuia.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Când mi se confirmă locul?
            </div>
            <div class="text">Imediat după ce plata este confirmată, un reprezentat te va contacta și anunța când trebuie să te prezinți la primul curs.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Cât costă un curs?
            </div>
            <div class="text">Prețul unui curs este variabil și este prezentat în pagina de descriere a fiecărui curs. De exemplu, pentru cursul HTML/CSS prețul este de 500 lei cu TVA.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Cum pot să plătesc?
            </div>
            <div class="text">Valoarea cursului se poate plăti direct online prin transfer bancar, sau prin depunere numerar la bancă în contul IBAN RO72 BTRL RONC RT03 0212 6201, deschis la Banca Transilvania, al societății Wise Projects Development SRL, CUI34633627 și J40/7037/2015</div>
        </div>
        <div class="faq">
            <div class="tab">
                Când au loc cursurile și cât durează?
            </div>
            <div class="text">Cursanții vor fi împărțiți în două serii a câte 20 de persoane, o serie având cursuri de dimineață, între 9 – 13, iar cealaltă după-amiază, între 14 -18, de luni până vineri.
                            Programa cursurilor se împarte pe o durată de 10 zile.
            </div>
        </div>
        <div class="faq">
            <div class="tab">
                Unde se vor ține cursurile?
            </div>
            <div class="text">Cursurile se vor ține în București, iar locația va fi comunicată in momentul confirmării.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Se va semna un contract între participanți și Școala de IT?
            </div>
            <div class="text">Da, fiecare participant în momentul în care se înscrie este de acord cu Contractul de Formare, ce se va semna după ce este efectuată plata.</div>
        </div>
        <div class="faq">
            <div class="tab">
                Primesc certificat / diplomă de absolvire la sfârșitul cursului?
            </div>
            <div class="text">La sfârșitul celor 10 zile de cursuri, participanții vor fi supuși unui test final prin care li se vor evalua cunoștințele dobândite. Cursanții ce vor promova acest test și au cel mult o absență, vor primi un certificat de participare/ diplomă emisă de Asociația Educație pentru Viată Reală. </div>
        </div>
        <div class="faq">
            <div class="tab">
                Am plătit cursul și doresc să renunț; cum pot face asta?
            </div>
            <div class="text">Dacă din diferite motive dorești să renunți după ți-a fost confirmat locul, atunci vei primi înapoi 50% din valoarea cursului, doar dacă renunți cu cel puțin 5 zile înainte de începerea cursurilor. 
                În cazul în care retragerea se face cu mai puțin de 5 zile înainte de începerea cursurilor, suma aferentă taxei de curs NU se returnează, deoarece locurile la curs sunt limitate și taxele reprezintă costurile pentru realizarea cursurilor și materialele de curs.
            </div>
        </div>

    </div>
</section>