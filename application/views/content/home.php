<div class="home_wrapper">
        <div class="hero">
            <div class="cell hero_text">
                <div class="text">Oportunități<br>profesionale</div>
            </div>
            <div class="cell">
                <img src="<?php echo site_url('static/assets/images/hero.svg') ?>" class="responsive_img" alt="">
            </div>
        </div>
        <div class="curs_promo_box">
            <div class="row">
                <div class="col-sm-7">
                    <div class="curs_name">CURS HTML/CSS</div>
                    <div class="curs_time">Durata: <strong>10 zile</strong></div>
                    <div class="curs_calendar"><a href="<?php echo site_url('calendar') ?>"><strong>Calendar curs</strong><i class="icon-right"></i></a></div>
                </div>
                <div class="col-sm-5">
                    <div class="curs_price">
                        <div class="curs_price_label">PREȚ PROMO</div>
                        500<span>RON</span>
                    </div>
                    <a href="#" data-toggle="modal" data-target="#modalInscriere" class="bt_pink">Vreau să mă înscriu <i class="icon-right"></i></a>
                </div>
            </div>
        </div>
    </div>