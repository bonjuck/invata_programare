<section class="section_content">
    <div class="container">
        <h4>CALENDAR INSCRIERI</h4>
        <div class="program_entry">
            <div class="table">
                <div class="col-md-4 cell">30.10 – 10.11</div>
                <div class="col-md-4 cell">ora 09:00 – 13:00  </div>
                <div class="col-md-4 cell"><a href="#" data-toggle="modal" data-target="#modalInscriere" data-time="30-10-2017 09:00" class="bt_pink">Inscrie-te <i class="icon-right"></i></a></div>
            </div>
        </div>
        <div class="program_entry">

            <div class="table">
                <div class="col-md-4 cell">30.10 – 10.11</div>
                <div class="col-md-4 cell">ora 14:00 – 18:00</div>
                <div class="col-md-4 cell"><a href="#" data-toggle="modal" data-target="#modalInscriere" data-time="30-10-2017 14:00" class="bt_pink">Inscrie-te <i class="icon-right"></i></a></div>
            </div>
        </div>
        <div class="program_entry">
            <div class="table">
                <div class="col-md-4 cell">13.11 – 24.11</div>
                <div class="col-md-4 cell">ora 09:00 – 13:00  </div>
                <div class="col-md-4 cell"><a href="#" data-toggle="modal" data-target="#modalInscriere" data-time="13-11-2017 09:00" class="bt_pink">Inscrie-te <i class="icon-right"></i></a></div>
            </div>
        </div>
        <div class="program_entry">
            <div class="table">
                <div class="col-md-4 cell">13.11 – 24.11</div>
                <div class="col-md-4 cell">ora 14:00 – 18:00</div>
                <div class="col-md-4 cell"><a href="#" data-toggle="modal" data-target="#modalInscriere" data-time="13-11-2017 14:00" class="bt_pink">Inscrie-te <i class="icon-right"></i></a></div>
            </div>
        </div>
    </div>
</section>