            
    <section class="section_content">
        <div class="container">
            <p class="big_text">Înveți să controlezi structura și estetica unui website/webapp</p>
            
            <div class="programa">
                <h4>PROGRAMA</h4> <!--<div class="bt_grey show_programa">Vezi detalii</div>-->
            </div>
            <div class="content_programa opened">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>Locul HTML si CSS in Internet of things</li>
                            <li>Procesul de design/dezvoltare</li>
                            <li>Structura unei pagini HTML(!DOCTYPE html, html, head, body)</li>
                            <li>Sintaxa HTML ( &lt;tag&gt; atribute=”value”>content&lt;/tag&gt;, entitati HTML)</li>
                            <li>Titluri, text, imagini, liste, link-uri – Prima pagina</li>
                            <li>Organizarea semantica a paginilor</li>
                            <li>Elemente de tip block si elemente de tip inline</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li>CSS - sintaxa si selectori</li>
                            <li>Sintaxa CSS</li>
                            <li>(selector {property: value;})</li>
                            <li>Selectori – tipuri, reguli de precedenta</li>
                            <li>Clase, Id, stilizarea unui element folosind clase multiple</li>
                            <li>Instrumente pentru dezvoltatori</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>Background</li>
                            <li>Culori</li>
                            <li>Imagini</li>
                            <li>Modelul caseta (box model)</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li>Tabele (tables)</li>
                            <li>Structura</li>
                            <li>Stiluri CSS pentru tabele</li>
                            <li>Gruparea datelor</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>Formulare (forms)</li>
                            <li>Interactiunea cu utilizatorul</li>
                            <li>Tipuri de elemente ale formurilor</li>
                            <li>Validarea continutului</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li>Layout</li>
                            <li>Pozitionarea elementelor</li>
                            <li>Cum sunt afisate elementele (display:..)?</li>
                            <li>Cum se pozitioneaza elementele?</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>Gradienti, multimedia si continut dinamic</li>
                            <li>Gradienti</li>
                            <li>Video</li>
                            <li>Audio</li>
                            <li>Tranzitii</li>
                            <li>Transformari</li>
                            <li>Animatie</li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
    </section>