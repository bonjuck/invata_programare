
<section class="section_content">
    <div class="container">
        <h4>Prin completarea FORMULARULUI DE ÎNSCRIERE acceptați următoarele termene și condiții: </h4>
        <h5>A.	FOLOSIREA DATELOR PERSONALE</h5>
        <p>Va fi considerată și va reprezenta consimțământul dumneavoastră expres și neechivoc pentru folosirea datelor personale în scopul contactării, transmiterii de mesaje, întocmirea de rapoarte, selectarea candidaților în scopul desfășurării procesului educațional și furnizarea informațiilor practice și utile pentru cariera viitoare a cursanților. Utilizatorul are obligația de a completa corect câmpurile formularului, în conformitate cu realitatea. Școala de IT nu își asumă răspunderea pentru corectitudinea datelor furnizate prin intermediul formularului. Dacă nu doriți ca datele dumneavoastră să fie colectate vă rugăm să nu ni le furnizați. Utilizarea în continuare a acestui site constituie acordul expres și neechivoc al dumneavoastră în conformitate cu prevederile Legii 677/2001 pentru protecția persoanelor cu privire la prelucrarea datelor cu caracter personal și libera circulație a acestor date. Pentru orice nelămurire în legătură cu exercitarea drepturilor dumneavoastră referitoare la utilizarea site-ului și la protecția utilizării vă rugăm să ne contactați prin intermediul secțiunii “Contact” din site. Termenii și condițiile de utilizare ale site-ului sunt guvernate și interpretate conform Legilor din România</p>
        <h5>B.	OBLIGAȚIILE CURSANȚILOR</h5>
        <p>Accesul in incinta spațiilor Școala de IT este admis numai în ținută decentă și numai în timpul programului stabilit. Cursantul trebuie sa păstreze ordinea, curățenia și să manifeste respect față de colegi, traineri, personal auxiliar.</p>
        <p>Sunt interzise:</p>
        <ul class="text_list">
            <li>Fumatul în incinta spațiilor. </li>
            <li>Prezentarea la programele de instruire în stare de ebrietate sau sub influența substanțelor halucinogene.</li>
            <li>Acte de vandalism, distrugere, deteriorarea mobilierului, aparaturii, echipamentelor.</li>
            <li>Împiedicarea procesului de învățământ, deranjarea orelor de curs.</li>
            <li>Nerespectarea obligațiilor de mai sus atrage exmatricularea persoanelor respective de la programele de formare profesională.</li>
        </ul>
        <h5>C.	OLIBAȚIILE ȘCOALA DE IT</h5>
        <p>Pentru a oferi informații actualizate Școala de IT va realiza periodic modificări în conținutul site-ului, schimbările referindu-se la informații despre cursuri, descrieri, poze, prețuri etc. Prețurile, durata și orarul cursurilor se pot modifica în funcție de costurile aferente unui curs sau alți factori.</p>
        <p>Școala de IT are obiectiv principal asigurarea celor mai bune condiții pentru desfășurarea procesului educațional și furnizarea informațiilor practice și utile pentru cariera viitoare a cursanților. Asigură acces facil, spații și dotări optime pentru desfășurarea cursurilor.</p>
        <p>Școala de IT pune la dispoziția cursanților suportul de curs și materialele aferente auxiliare atunci când este cazul.</p>
        <h5>D.	CONTRACTUL DE FORMARE</h5>
        <p>Prin acceptarea acestor termene și condiții sunteți de acord cu semnarea Contractului de Formare, ce va fi semnat ulterior și în format fizic după efectuarea plății și confirmarea locului în cadrului unui curs organizat de Școala de IT.</p>
        <h5>E.	DISPOZIȚII FINALE</h5>
        <p>Prin utilizarea site-ului, trimiterea formularului sau participarea la cursurile organizate se consideră că ați citit, înțeles și acceptat prezenții Termeni și Condiții. Școala de IT poate modifica, revizui, îmbunătăți sau întrerupe furnizarea de informații, produse sau servicii prezentate pe acest site sau oricare din documentele incluse pe acest site, fără a avea obligația de a informa în prealabil in acest sens.</p>
        
    </div>
</section>