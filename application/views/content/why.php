<div class="dece_content">
<!--    <div class="title">
        <div class="big">DE CE</div>
        <div class="small">INVATAPROGRAMARE.RO</div>
    </div>-->
    <div class="responses_container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">Dezvolți abilități care se caută</div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">Interacționezi cu potențiali angajatori</div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">Întâlnesti experți în domeniu</div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">Ai șansa de a câștiga unul din cele 5 internshipuri la companiile partenere</div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">40 ore de curs</div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">10 proiecte finalizate</div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">Înveți făcând</div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">Cunoști alți oameni interesați ca și tine de domeniu</div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">Începi cariera în web development cu o bază solidă</div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
<!--        <ul>
            <li>Dezvolți abilități care se caută</li>
            <li>Interacționezi cu potențiali angajatori</li>
            <li>Întâlnesti experți în domeniu</li>
            <li>Ai șansa de a cîștiga unul din cele 5 internshipuri la parteneri</li>
            <li>40 ore de curs</li>
            <li>10 proiecte finalizate</li>
            <li>Învățăm făcând</li>
            <li>Cunoști alți oameni interesați ca și tine de domeniu</li>
            <li>Începi cariera în webdevelopment cu o bază solidă</li>
        </ul>-->

    </div>
</div>