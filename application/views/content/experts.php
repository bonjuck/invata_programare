
<section class="section_content">
    <div class="container">
        <div class="row expert">
            <div class="col-md-3 col-sm-4 col-lg-4 image">
                <img src="<?php echo site_url('static/assets/images/experti/AA.jpg') ?>" alt="">
            </div>
            <div class="col-md-9 col-sm-8 col-lg-8 description">
                <div class="name">Adrian Apostol</div>
                <div class="expertiza">Founder & Managing Partner Ideologiq Group</div>
                <p>Adrian Apostol este fondator și partener în cadrul agenției Ideologiq. Are o experiență de peste 17 ani în domeniul digital și a coordonat proiecte majore pe piața locală și internațională. 
                    A lucrat în calitate de manager de proiect pentru Netbridge, compania numărul 1 de internet din România la începutul anilor 2000. În 2006 s-a alăturat echipei de lucru de la Apropo Media unde a coordonat proiecte precum Sport.ro și Profesiionline.ro. 
                    Din 2007, în calitate de antreprenor, a colaborat cu cele mai importante agenții de publicitate din România (Ogilvy, Draft FCB, Publicis, Momentum). În 2011 a fondat împreună cu Adrian Ichim agenția Advanced Ideas. A coordonat un număr mare de proiecte pentru clienți mari precum Bucovina, GSK, Allegro, Bricodepot, Medlife și Michelin. 
                    În 2017 a lansat Ideologiq după un proces de rebranding al agenției Advanced Ideas. Acum, coordonează procesul de lansare a companiei în regiunea din apropiere (Grecia, Ungaria, Serbia și Bulgaria) și dezvoltă strategia de creștere pe termen lung și mediu, atât local, cât și internațional.
                </p>
            </div>
        </div>
        <div class="row expert">
            <div class="col-md-3 col-sm-4 col-lg-4 image">
                <img src="<?php echo site_url('static/assets/images/experti/Adi_Ichim.png') ?>" alt="">
            </div>
            <div class="col-md-9 col-sm-8 col-lg-8 description">
                <div class="name">Adrian Ichim</div>
                <div class="expertiza">Founder & Managing Partner Ideologiq Group</div>
                    <p>Adrian Ichim este co-fondator și partener al Agenției Ideologiq, fondator al serviciului Mainframe Development și Mainframe Service, fiind implicat în prezent în dezvoltarea de strategii și concepte creative pentru clienții din portofoliul agenției. 
                        Adrian și-a început cariera în domeniul publicității digitale ca antreprenor, fondându-și primul start-up în 2003. Cea mai nouă provocare antreprenorială îl reprezintă lansarea Agenției Ideologiq în Europa Centrală și de Est (România, Bulgaria, Grecia, Ungaria, Serbia, Croația, Cehia, Slovacia și Polonia ). 
                        În cei 14 ani de activitate, a gestionat echipe de peste 40 de persoane și a participat activ la crearea și lansarea a peste 400 de proiecte care acoperă o întreaga gamă de comunicare digitală, fie web, mobil, social sau search. 
                        Adrian a fost de asemenea implicat și în diverse procese de digitalizare pentru unele dintre cele mai mari organizații locale și globale, cum ar fi Allegro, Ficosota, Diageo, Ringier, Holcim, GSK, Pfizer, JTI, Nestle, Medlife, Dreamstime, Porsche, Petrom sau Raiffeisen.
                    </p>    
            </div>
        </div>
<!--        <div class="row expert">
            <div class="col-md-3 col-sm-4 col-lg-4 image">
                <img src="<?php echo site_url('static/assets/images/experti/expert.jpg') ?>" alt="">
            </div>
            <div class="col-md-9 col-sm-8 col-lg-8 description">
                <div class="name">Remus Munteanu</div>
                <div class="expertiza">CTO</div>
                <p>Fusce ac nulla ut purus ultricies molestie a a dui. Nullam convallis massa eget quam vehicula.
                    sagittis. Aenean bibendum malesuada tristique. Vivamus nunc enim, consectetur id nibh ac,
                    suscipit adipiscing leo. Donec diam tellus, pharetra at sodales in, consectetur porttitor odio. Sed
                    laoreet tempus neque at accumsan. Morbi urna felis, mattis ac pretium at, sollicitudin sed arcu.
                    Sed faucibus, leo eu facilisis eleifend, ipsum
                </p>
            </div>
        </div>-->
        <div class="row expert">
            <div class="col-md-3 col-sm-4 col-lg-4 image">
                <img src="<?php echo site_url('static/assets/images/experti/Mihai_Radu.jpg') ?>" alt="">
            </div>
            <div class="col-md-9 col-sm-8 col-lg-8 description">
                <div class="name">Mihai Radu</div>
                <div class="expertiza">HTML Developer</div>
                <p>Developer entuziast, Mihai transforma viziunile importante si ideile inteligente in pagini web fara cusur, folosind cele mai noi tehnologii in dezvoltarea web.
                    Pasionat de flat design, obsedat de perfect pixel, Mihai este dornic sa impartaseasca cu noi din experienta acumulata in zecile de proiecte in care a fost implicat.
                </p>
            </div>
        </div>
    </div>
</section>